import React from 'react';
import { View, Image, StyleSheet, ScrollView } from 'react-native';

import InfoPanel from '../components/InfoPanel';
import Rating from '../components/Rating';
import Calendar from '../components/Calendar';

import theme from '../theme';
import { faStar, faClock, faMapMarkerAlt, faRuler } from '@fortawesome/free-solid-svg-icons';
import i18n from '../localization/i18n';


const printHour = (startHour, endHour) => {
    if (startHour == 0 && endHour == 24) {
        return "24h/24";
    }
    return startHour + "h - " + endHour + "h";
}

const DetailScreen = props => {
    const data = props.route.params.data;


    return (
        <ScrollView style={styles.container}>
            <View style={styles.imgContainer}>
                <Image style={styles.img} source={{ uri: data.img }} />
            </View>

            <Rating
                reviews={[i18n.t('detail.common'), i18n.t('detail.fairlyCommon'), i18n.t('detail.uncommon'), i18n.t('detail.scarce'), i18n.t('detail.veryRare')]}
                color={theme.YELLOW_COLOR}
                icon={faStar}
                count={5}
                ratingCount={data.rarity}
                size={35}
            />

            <View style={styles.infoPanelContainer}>
                <InfoPanel color={theme.YELLOW_COLOR} title={data.price.toString()} icon={faStar} size={22} />
                <InfoPanel color={theme.BLUE_COLOR} title={printHour(data.startHour, data.endHour)} icon={faClock} size={22} />
                <InfoPanel color={theme.RED_COLOR} title={data.location} icon={faMapMarkerAlt} size={22} />

            </View>
            {data.shadowSize ?
                (<View style={styles.infoPanelContainer}>
                    <InfoPanel color={theme.PRIMARY_COLOR} title={data.shadowSize} icon={faRuler} size={22} />
                </View>) : null}

                <Calendar style={{paddingVertical: 15}} months={data.months} />
        </ScrollView>
    );
};

export default DetailScreen;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.WHITE_COLOR,
        padding: 10,
    },
    infoPanelContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 15,
    },
    imgContainer: {
        alignItems: 'center',
        padding: 10,
    },
    img: {
        flex: 1,
        minHeight: 120,
        minWidth: 120,
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        resizeMode: 'contain'
    },
    shadow: {
        alignItems: 'center',
    },
})