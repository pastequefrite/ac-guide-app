import React, { useEffect, useRef } from 'react';
import { View, StyleSheet, FlatList, Image, Text } from 'react-native';
import { ListItem } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast'

import LoadingActivity from '../components/LoadingActivity';
import separator from '../components/Separator';

import { connect } from 'react-redux';
import { getAnimals, getResults } from '../api';

import theme from '../theme';

import i18n from '../localization/i18n';


const ListScreen = props => {
    const toastRef = useRef();


    useEffect(() => {
        props.fetchData();
    }, [])

    useEffect(() => {
        displayError();
    }, [props.result])

    const displayError = () => {
        if (props.result && props.result.error && toastRef.current) {
            toastRef.current.show("Error: " + props.result.errorPayload.message);
        }
    };

    if (!props.result || props.result.fetching) {
        return <LoadingActivity />
    };

    return (
        <View style={styles.container}>
            <Toast ref={toastRef} style={styles.toast} position='top' positionValue={20} />
            <FlatList
                data={props.result.successPayload}
                renderItem={({ item }) => (
                    <ListItem
                        title={item.name}
                        leftAvatar={<Image source={{ uri: item.img }} style={styles.img} />}
                        containerStyle={{ borderBottomWidth: 0 }}
                        chevron
                        onPress={() => props.navigation.navigate('Detail', { name: item.name, data: item })}
                    />
                )}
                contentContainerStyle={!props.result.successPayload || props.result.successPayload.length === 0 ? styles.center : null}
                keyExtractor={item => item.id.toString()}
                ItemSeparatorComponent={separator}
                ListEmptyComponent={
                    <View style={styles.center}><Text style={styles.noData}>{i18n.t('list.noDataFound')}</Text></View>
                }
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    img: {
        width: 40,
        height: 40
    },
    toast: {
        backgroundColor: theme.RED_COLOR,
        borderRadius: 10,
    }
});


// const CACHE_KEY = 'GET/animals_' + props.route.params.category;

const enhance = connect(
    (state, ownProps) => ({
        result: getResults(state, 'GET/animals_' + ownProps.route.params.category)
    }),
    (dispatch, ownProps) => ({
        fetchData() {
            return dispatch(
                getAnimals('GET/animals_' + ownProps.route.params.category, { category: ownProps.route.params.category })
            );
        }
    })
);

export default enhance(ListScreen);