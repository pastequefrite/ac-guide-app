import moment from 'moment';
import 'moment/min/locales';
import * as RNLocalize from 'react-native-localize';

const currentLocales = RNLocalize.getLocales();
moment.locale(currentLocales[0].languageCode);

export default moment;