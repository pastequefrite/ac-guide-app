import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import theme from '../theme';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import PropTypes from 'prop-types';


const SpringboardButton = props => {

    return (
        <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.button} onPress={props.onPress}>
                <FontAwesomeIcon icon={props.icon} size={50} color="white" />
            </TouchableOpacity>
            <Text style={styles.buttonTitle}>{props.title}</Text>
        </View>
    );
};

SpringboardButton.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.object.isRequired,
}

const styles = StyleSheet.create({
    buttonContainer: {
        // padding: 20,
    },

    button: {
        width: 80,
        height: 80,
        backgroundColor: theme.PRIMARY_COLOR,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },

    buttonTitle: {
        textAlign: 'center',
    }
});

export default SpringboardButton;
