import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import theme from '../theme';


const Rating = props => {
    const rate = () => {
        let rate = []
        for (let i = 0; i < props.count; i++) {
            rate.push(<FontAwesomeIcon key={i} icon={props.icon} size={props.size} color={props.color} style={[styles.icon, { opacity: 0.5 }]} />)
        }
        for (let i = 0; i < props.ratingCount; i++) {
            rate[i] = <FontAwesomeIcon key={i} icon={props.icon} size={props.size} color={props.color} style={styles.icon} />
        }
        return rate;
    };

    return (
        <View style={styles.container}>
            <View style={styles.rate}>{rate()}</View>
            <Text style={[styles.title,{color: props.color}]}>{props.reviews[props.ratingCount - 1]}</Text>
        </View>
    );
};

Rating.propTypes = {
    icon: PropTypes.object.isRequired,
    reviews: PropTypes.arrayOf(PropTypes.string),
    size: PropTypes.number,
    color: PropTypes.string,
    ratingCount: PropTypes.number,
    count: PropTypes.number.isRequired,
    title: PropTypes.string,
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center'
    },
    rate: {
        flexDirection: 'row',
    },
    title: {
        textAlign: 'center',
        fontSize: theme.FONT_SIZE_L,
        color: theme.BLACK_COLOR,
        padding: 5,
    },
    icon: {
        marginHorizontal: 5,
    }
})

export default Rating;