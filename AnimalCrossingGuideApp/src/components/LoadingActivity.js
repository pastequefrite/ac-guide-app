import React from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';

const LoadingActivity = props => {
    return (
        <View style={[styles.container]}>
            <ActivityIndicator/>
        </View>);
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});

export default LoadingActivity;
